var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1200",
        "ok": "1193",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "1867",
        "ok": "1867",
        "ko": "3418"
    },
    "maxResponseTime": {
        "total": "5281",
        "ok": "4824",
        "ko": "5281"
    },
    "meanResponseTime": {
        "total": "2099",
        "ok": "2084",
        "ko": "4774"
    },
    "standardDeviation": {
        "total": "343",
        "ok": "272",
        "ko": "617"
    },
    "percentiles1": {
        "total": "2007",
        "ok": "2007",
        "ko": "5112"
    },
    "percentiles2": {
        "total": "2059",
        "ok": "2057",
        "ko": "5203"
    },
    "percentiles3": {
        "total": "2535",
        "ok": "2492",
        "ko": "5277"
    },
    "percentiles4": {
        "total": "3891",
        "ok": "3533",
        "ko": "5280"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1193,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.704",
        "ok": "3.682",
        "ko": "0.022"
    }
},
contents: {
"req_post--recogidas-ee2aa": {
        type: "REQUEST",
        name: "POST /recogidas/cm-solicitud-recogidas-ms/solicitud-recogida",
path: "POST /recogidas/cm-solicitud-recogidas-ms/solicitud-recogida",
pathFormatted: "req_post--recogidas-ee2aa",
stats: {
    "name": "POST /recogidas/cm-solicitud-recogidas-ms/solicitud-recogida",
    "numberOfRequests": {
        "total": "1200",
        "ok": "1193",
        "ko": "7"
    },
    "minResponseTime": {
        "total": "1867",
        "ok": "1867",
        "ko": "3418"
    },
    "maxResponseTime": {
        "total": "5281",
        "ok": "4824",
        "ko": "5281"
    },
    "meanResponseTime": {
        "total": "2099",
        "ok": "2084",
        "ko": "4774"
    },
    "standardDeviation": {
        "total": "343",
        "ok": "272",
        "ko": "617"
    },
    "percentiles1": {
        "total": "2007",
        "ok": "2007",
        "ko": "5112"
    },
    "percentiles2": {
        "total": "2059",
        "ok": "2057",
        "ko": "5203"
    },
    "percentiles3": {
        "total": "2535",
        "ok": "2492",
        "ko": "5277"
    },
    "percentiles4": {
        "total": "3891",
        "ok": "3533",
        "ko": "5280"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1193,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 7,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.704",
        "ok": "3.682",
        "ko": "0.022"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
