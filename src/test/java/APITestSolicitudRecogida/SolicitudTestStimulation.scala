package APITestSolicitudRecogida

import com.intuit.karate.gatling.KarateProtocol
import com.intuit.karate.gatling.PreDef._
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder

import scala.concurrent.duration._

class SolicitudTestStimulation extends Simulation {

  val protocol: KarateProtocol = karateProtocol()

  val solicitarRecogida: ScenarioBuilder = scenario("Solicitar recogida").exec(karateFeature("classpath:APITestSolicitudRecogida/PostSolicitudRecogidaAPI.feature"))

  setUp(solicitarRecogida.inject(rampUsers(60) during(30 seconds)).protocols(protocol))

}
